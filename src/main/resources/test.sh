#!/bin/bash
# In order to make bad request pass some character or alphanum string as first arg of this script
OUTPUT_FILE=tmp.test.res
TYPE="$1"
shift
if [ "$TYPE" != "response" ] && [ "$TYPE" != "recent" ] && [ "$TYPE" != "inactive" ] && [ "$TYPE" != "conf" ]; then
  echo "incorrect request type must be 'response' or 'recent' ";
  exit 1
fi

while [[ $# -gt 0 ]]; do
  case $1 in
    -p|--url-pollution)
      URL_POLLUTION="$2"
      shift 2;
      ;;
    -P|--param-pollution)
      PARAM_POLLUTION="$2"
      shift 2;
      ;;
    -D|--data-pollution)
      DATA_POLLUTION="$2"
      shift 2;
      ;;
    -n|--no-conf)
      POST_CONF=false
      shift;
      ;;
    -O|--only-conf)
      ONLY_CONF=true
      shift;
      ;;
    -c|--conf-pollution)
        CONF_POLLUTION="$2"
        shift 2;
        ;;
    -o|--output-file)
      OUTPUT_FILE="$2"
      shift 2
      ;;
    -*)
      echo "Unknown option $1"
      exit 1
      ;;
  esac
done

if ! [ "$POST_CONF" = "false" ]; then
  echo "posting configuration"
  curl -o"$OUTPUT_FILE" -i -X POST localhost:8080/config \
    -H 'Content-type:application/json' -d "$CONF_POLLUTION$(cat ./example-config.json)"
fi

if [ "$TYPE" = "conf" ]; then
  exit 0
fi

echo $'\n\n' >> "$OUTPUT_FILE"
if [ "$TYPE" = "response" ]; then
  echo "getting data"
  curl -i -X GET localhost:8080/response -G \
    -d "url=https://datahub.ki.agh.edu.pl/pl/datasets/env-mon-outskirts-of-krakow/endpoints/87$URL_POLLUTION/data/" \
    -d "from=2021-04-05T00:00:00%2b02:00" \
    -d "to=2022-05-09T00:00:00%2b02$DATA_POLLUTION:00" \
    -d "param=particleConcentrationSensor/concentration$PARAM_POLLUTION/pm1" >> "$OUTPUT_FILE"

elif [ "$TYPE" = "recent" ]; then
   curl -i -X GET localhost:8080/recent >> "$OUTPUT_FILE"
elif [ "$TYPE" = "inactive" ]; then
   curl -i -X GET localhost:8080/inactive >> "$OUTPUT_FILE"
fi

