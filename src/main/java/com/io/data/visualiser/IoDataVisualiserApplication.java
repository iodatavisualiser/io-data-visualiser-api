package com.io.data.visualiser;

import com.io.data.visualiser.model.vpn.VpnConnector;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IoDataVisualiserApplication {
    public static void main(String[] args) {
        Thread vpnConnector = new Thread(new VpnConnector());
        vpnConnector.start();
        SpringApplication.run(IoDataVisualiserApplication.class, args);
    }

}
