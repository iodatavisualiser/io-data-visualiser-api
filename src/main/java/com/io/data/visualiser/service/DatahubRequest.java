package com.io.data.visualiser.service;

import com.io.data.visualiser.model.cache.Cache;
import com.io.data.visualiser.model.configuration.ConfigurationProvider;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Setter;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Date;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;

// example link:: https://datahub.ki.agh.edu.pl/api/endpoints/73/data/?limit=25&offset=25

public abstract class DatahubRequest implements Callable<DatahubRequest.DhReqResult> {
    @Setter private static int DEFAULT_TIMEOUT_MILIS = 10000;
    static final Logger LOGGER = LoggerFactory.getLogger(DatahubRequest.class);
    static final String DATAHUB_DATA_CAST_EXCEPTION_MESSAGE = "probably unexpected format of datahub data";
    static final String DATA_FIELD = "data";
    static final String RESULTS_FIELD = "results";
    static final String TIMESTAMP_FIELD = "timestamp";
    static final String ID_FIELD = "id";
    static final String BASE_URL = "https://datahub.ki.agh.edu.pl/";


    final Cache cache;
    final String[] dataPointers;
    final String path;
    Duration timeout = Duration.ofMillis(DEFAULT_TIMEOUT_MILIS);
    List<Throwable> errors;

    DatahubRequest(String[] dataPointers, String endpoint) {
        if (ConfigurationProvider.getConfiguration() != null) {
            this.cache = ConfigurationProvider.getEndpointCaches().getCache(endpoint);
        } else {
            this.cache = null;
        }
        this.dataPointers = dataPointers;
        if (!endpoint.startsWith(BASE_URL)) {
            String message = "endpoint has suspected baseUrl :<" + endpoint + ">";
            LOGGER.info(message);
            throw new IllegalArgumentException(message);
        }
        this.path = endpoint;
        this.errors = new LinkedList<>();
    }

    @Override
    abstract public DhReqResult call() throws Exception;

    void handleMeasurement(Object measurement_) {
        if (cache != null) {
            try {
                JSONObject measurement = (JSONObject) measurement_;
                ZonedDateTime date = ZonedDateTime.parse((String) measurement.get(TIMESTAMP_FIELD));
                JSONObject data = (JSONObject) measurement.get(DATA_FIELD);
                for (String dataPointer : dataPointers) {
                    String value = searchForDesiredSensorData(data, dataPointer);
                    if (value != null) {
                        cache.add(
                                dataPointer,
                                Date.from(date.toInstant()),
                                value);
                    }

                }
            } catch (ClassCastException e) {
                String message = "measurement parsing:" + DATAHUB_DATA_CAST_EXCEPTION_MESSAGE + ":: caused by : " + e.getMessage();
                LOGGER.error(message);
                errors.add(new DatahubReqException(message, e));
            }
        } else {
            LOGGER.warn("cache is not set");
        }
    }

    String getID(JSONArray results) {
        for (Object r : results) {
            try {
                String id =
                        (String) (
                                (JSONObject)
                                        ((JSONObject) r).get(DATA_FIELD)
                        ).get(ID_FIELD);

                if (id != null) {
                    return id;
                }
            } catch (ClassCastException | NullPointerException e) {
                LOGGER.error(e.getMessage());
                errors.add(new DatahubReqException("error during getting ID from datahub response", e));
            }
        }
        return null;
    }

    JSONArray acquireResults(String path, int limit, int offset, Duration timeout) {
        try {
            JSONObject json = getJSONObjectFormDatahub(path, limit, offset, timeout);
            return (JSONArray) json.get(RESULTS_FIELD);
        } catch (ClassCastException e) {
            String message = "results parsing:" + DATAHUB_DATA_CAST_EXCEPTION_MESSAGE + ":: caused by : " + e.getMessage();
            LOGGER.error(message);
            errors.add(new DatahubReqException(message, e));
            return null;
        } catch (DatahubReqException e) {
            return null;
        }
    }

    JSONObject getJSONObjectFormDatahub(String path, int limit, int offset, Duration timeout) throws DatahubReqException {
        try {
            WebClient webClient = WebClient.create(BASE_URL);
            WebClient.ResponseSpec resSpec = webClient.get()
                    .uri(buildPath(path, limit, offset))
                    .retrieve();
            String response = resSpec
                    .bodyToMono(String.class)
                    .timeout(timeout)
                    .block();

            if (response == null) {
                String message = "response is null";
                LOGGER.error(message);
                DatahubReqException error = new DatahubReqException(message);
                errors.add(error);
                throw error;
            }
            return (JSONObject) JSONValue.parse(response);
        } catch (Exception e) {
            String message = "could not communicate with server";
            LOGGER.error(message);
            DatahubReqException error = new DatahubReqException(message);
            errors.add(error);
            throw error;
        }
    }

    String searchForDesiredSensorData(JSONObject data, String dataPointer) {
        try {
            Object tmp = data;
            String[] fieldsNestingInJsonObject = dataPointer.trim().split("/");
            for (String step : fieldsNestingInJsonObject) {
                if (tmp == null)
                    return null;
                tmp = ((JSONObject) tmp).get(step);
            }
            if (tmp == null)
                return null;
            return String.valueOf(tmp);
        } catch (ClassCastException e) {
            LOGGER.warn("Data not found for: " + dataPointer);
            errors.add(new DatahubReqException("error during getting data from datahub response", e));
            return null;
        }
    }

    static String buildPath(String endpoint, int limit, int offset) {
        return String.format("%s/?limit=%d&offset=%d", endpoint, limit, offset);
    }

    @Data
    @AllArgsConstructor
    public static class DhReqResult {
        String id;
        List<Throwable> errors;
    }

}
