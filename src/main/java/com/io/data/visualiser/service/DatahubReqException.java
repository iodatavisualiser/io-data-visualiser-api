package com.io.data.visualiser.service;

public class DatahubReqException extends Exception{
    public DatahubReqException(String message) {
        super(message);
    }
    public DatahubReqException(String message, Throwable cause) {
        super(message, cause);
    }
    public DatahubReqException(Throwable cause) {
        super(cause);
    }
}
