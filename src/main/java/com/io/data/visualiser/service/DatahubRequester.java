package com.io.data.visualiser.service;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


public class DatahubRequester {
    private static final int CONCURRENCY = 4;
    static ExecutorService requestsExecutor = Executors.newFixedThreadPool(CONCURRENCY);

    private DatahubRequester() { }

    public static Future<DatahubRequest.DhReqResult> req(String[] dataPointers, String endpoint,
                                                         int limit, int offset) {
        return requestsExecutor.submit(
                new LimitOffsetDatahubRequest(
                        dataPointers,
                        endpoint,
                        limit,
                        offset)
        );

    }

    public static Future<DatahubRequest.DhReqResult> req(String[] dataPointer, String endpoint,
                                                         Date dateFrom, Date dateTo) {
        ZonedDateTime dateFrom_ = ZonedDateTime.ofInstant(dateFrom.toInstant(), ZoneId.systemDefault());
        ZonedDateTime dateTo_ = ZonedDateTime.ofInstant(dateTo.toInstant(), ZoneId.systemDefault());

        return requestsExecutor.submit(
                new DateRangeDatahubRequest(
                        dataPointer,
                        endpoint,
                        dateFrom_,
                        dateTo_)
        );
    }

    public static Future<DatahubRequest.DhReqResult> req(String[] dataPointer, String endpoint,
                                                         ZonedDateTime dateFrom, ZonedDateTime dateTo) {
        return requestsExecutor.submit(
                new DateRangeDatahubRequest(
                        dataPointer,
                        endpoint,
                        dateFrom,
                        dateTo)
        );
    }


    static public void shutdown() {
        requestsExecutor.shutdown();
    }
    static public void restart() {
        if (!DatahubRequester.requestsExecutor.isShutdown()) {
            DatahubRequester.requestsExecutor.shutdown();
        }
        DatahubRequester.requestsExecutor = Executors.newFixedThreadPool(CONCURRENCY);
    }
}
