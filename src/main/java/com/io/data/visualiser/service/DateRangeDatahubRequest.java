package com.io.data.visualiser.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.time.ZonedDateTime;
import java.util.List;


public class DateRangeDatahubRequest extends DatahubRequest {
    private final int offsetStep = 200;

    private final ZonedDateTime dateFrom;
    private final ZonedDateTime dateTo;

    private ZonedDateTime currentUpperDate;
    private ZonedDateTime currentLowerDate;

    DateRangeDatahubRequest(String[] dataPointers, String endpoint,
                            ZonedDateTime dateFrom, ZonedDateTime dateTo) {
        super(dataPointers, endpoint);
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }

    @Override
    public DhReqResult call() throws Exception {
        int offset = 0;
        String id = null;

        while (true) {
            try {
                JSONArray results = acquireResults(path, offsetStep, offset, timeout);
                if (stopCondition(results)) {
                    break;
                }
                if (id == null) {
                    id = getID(results);
                }
                for (Object measurement : results) {
                    if (checkDate(measurement)) {
                        handleMeasurement(measurement);
                    }
                }
                offset += offsetStep;
            } catch (Exception e) {
                LOGGER.error(e.getMessage());
                break;
            }
        }
        return new DhReqResult(id, errors);
    }

    boolean stopCondition(JSONArray results) {
        return results == null ||
                results.size() == 0 ||
                OffsetState.TOO_FAR.equals(currentState(results));
    }

    private OffsetState currentState(JSONArray results) {
        currentUpperDate = ZonedDateTime.parse(
                (String) (
                        ((JSONObject) results.get(0)).get(TIMESTAMP_FIELD))
        );
        currentLowerDate = ZonedDateTime.parse(
                (String) (
                        (JSONObject)results.get(results.size() - 1)
                ).get(TIMESTAMP_FIELD));

        if (currentLowerDate.isAfter(dateTo)) {
            return OffsetState.MOVE;
        } else if (currentUpperDate.isBefore(dateFrom)) {
            return OffsetState.TOO_FAR;
        } else return OffsetState.OVERLAPPING;
    }

    private boolean checkDate(Object measurement) {
        ZonedDateTime date = ZonedDateTime.parse((String) ((JSONObject) measurement).get(TIMESTAMP_FIELD));
        return date.isBefore(currentUpperDate) && date.isAfter(currentLowerDate);
    }



    private enum OffsetState {
        TOO_FAR,
        OVERLAPPING,
        MOVE
    }

}
