package com.io.data.visualiser.service;

import org.json.simple.JSONArray;


public class LimitOffsetDatahubRequest extends DatahubRequest {
    private final int limit;
    private final int offset;

    LimitOffsetDatahubRequest(String[] dataPointers, String endpoint,
                                      int limit, int offset) {
        super(dataPointers, endpoint);
        this.limit = limit;
        this.offset = offset;
    }

    @Override
    public DhReqResult call() throws Exception {
        JSONArray results = acquireResults(path, limit, offset, timeout);

        for (Object measurement : results) {
            handleMeasurement(measurement);
        }
        return new DhReqResult(getID(results), errors);
    }

}
