package com.io.data.visualiser.model.cache;

import com.io.data.visualiser.model.configuration.ConfigurationProvider;
import com.io.data.visualiser.model.structure.data.EndpointData;
import com.io.data.visualiser.model.structure.data.PairEndpointData;
import com.io.data.visualiser.model.structure.data.Station;
import com.io.data.visualiser.model.structure.response.CacheDataResponse;
import com.io.data.visualiser.model.structure.response.CacheInactiveListResponse;
import com.io.data.visualiser.service.DatahubRequest;
import com.io.data.visualiser.service.DatahubRequester;
import com.io.data.visualiser.model.structure.response.CacheRecentDataResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class EndpointCaches {
    static final Logger LOGGER = LoggerFactory.getLogger(EndpointCaches.class);
    private final Map<String,Cache> caches;

    public EndpointCaches(List<String> urls, List<String> params) {
        caches = new HashMap<>();
        for(String s : urls){
            caches.put(s, new Cache(params, s));
        }
    }

    public Cache getCache(String url) {
        return caches.get(url);
    }

    public CacheDataResponse getData(String url, Date from, Date to, String parameter) {
        if (caches.get(url) == null)
            throw new IllegalArgumentException("no url in conf:: " + url);
        return caches.get(url).getData(parameter, from, to);
    }

    public CacheRecentDataResponse getRecent() {
        List<Throwable> errors = new LinkedList<>();
        List<EndpointData> data = new ArrayList<>(caches.size());
        List<Future<DatahubRequest.DhReqResult>> calls = new ArrayList<>(caches.size());
        for(Station stations: ConfigurationProvider.getConfiguration().getStations()) {
            calls.add(DatahubRequester.req(
                    ConfigurationProvider.getConfiguration().getSensorNameArray(),
                    stations.getEndpoint(),
                    1,
                    0)
            );
        }
        for(Future<DatahubRequest.DhReqResult> n : calls){
            try {
                n.get();
                errors = n.get().getErrors();
            } catch (InterruptedException | ExecutionException e) {
                LOGGER.error(e.getMessage());
            }
        }
        for(String name: caches.keySet()){
            data.add(new EndpointData(name, caches.get(name).getRecent()));
        }
        return new CacheRecentDataResponse(
                data,
                errors
                    .stream()
                    .map(Throwable::getMessage)
                    .toArray(String[]::new),
                errors);
    }

    public CacheInactiveListResponse getInactive() {
        List<Throwable> errors = new LinkedList<>();
        List<Future<DatahubRequest.DhReqResult>> calls = new ArrayList<>(caches.size());
        for (Station stations: ConfigurationProvider.getConfiguration().getStations()) {
            if (caches.get(stations.getEndpoint())
                    .getLastDate()
                    .before(Date.from(
                            LocalDateTime.now().minusDays(1).atZone(ZoneId.systemDefault())
                                    .toInstant()))) {
                calls.add(DatahubRequester.req(ConfigurationProvider.getConfiguration().getSensorNameArray(), stations.getEndpoint(),
                        1, 0));
            }
        }
        for (Future<DatahubRequest.DhReqResult> n : calls) {
            try {
                n.get();
                errors.addAll(n.get().getErrors());
            } catch (InterruptedException | ExecutionException e) {
                LOGGER.error(e.getMessage());
            }
        }
        CacheInactiveListResponse cacheInactiveListResponse = new CacheInactiveListResponse();
        for(Station stations: ConfigurationProvider.getConfiguration().getStations()) {
            Date d = caches.get(stations.getEndpoint()).getLastDate();
            if (d.before(Date.from(LocalDateTime.now().minusDays(1).atZone(ZoneId.systemDefault()).toInstant()))) {
                cacheInactiveListResponse.addPair(new PairEndpointData(stations.getName(), stations.getEndpoint(),d));
            }
        }
        cacheInactiveListResponse.setErrors(errors);
        cacheInactiveListResponse.setErrorsMessages(
                errors
                        .stream()
                        .map(Throwable::getMessage)
                        .toArray(String[]::new));
        return cacheInactiveListResponse;
    }

}
