package com.io.data.visualiser.model.vpn;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;

public class VpnConnector implements Runnable{

    public void run()
    {
        Path resourceDirectory = Paths.get("src","main","resources","VPN-AGH.2022.ovpn");
        String absolutePath = resourceDirectory.toFile().getAbsolutePath();
        StringBuilder result = new StringBuilder(80);
        try
        {
            ProcessBuilder pb = new ProcessBuilder("openvpn", absolutePath).redirectErrorStream(true);
            Process process = pb.start();
            try (BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream())))
            {
                while (true)
                {
                    String line = in.readLine();
                    if (line == null)
                        break;
                    result.append(line).append(System.getProperty("line.separator"));
                }
            }
        }
        catch (IOException exception)
        {
            exception.printStackTrace();
        }
        System.out.println(result.toString());
    }
}
