package com.io.data.visualiser.model.structure.data;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
public class DateInterval {
    public Date from;
    public Date to;

}
