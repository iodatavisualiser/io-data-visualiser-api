package com.io.data.visualiser.model.configuration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.io.data.visualiser.model.configuration.Configuration;

import java.io.FileNotFoundException;


public class ConfigFileReader {
    static public Configuration readConfiguration(String file) throws FileNotFoundException, JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(file, Configuration.class);
    }
}
