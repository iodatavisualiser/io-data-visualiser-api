package com.io.data.visualiser.model.structure.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.io.data.visualiser.model.structure.data.PairEndpointData;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.LinkedList;
import java.util.List;

@Data
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CacheInactiveListResponse implements ErrorsHandlingResponseI {
    List<PairEndpointData> data;
    String[] errorsMessages;
    @JsonIgnore
    List<Throwable> errors;


    public CacheInactiveListResponse() {
        data = new LinkedList<>();
    }

    public void addPair(PairEndpointData pair) {
        data.add(pair);
    }
}
