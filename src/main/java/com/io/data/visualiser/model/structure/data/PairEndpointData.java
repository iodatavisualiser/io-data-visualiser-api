package com.io.data.visualiser.model.structure.data;

import lombok.Data;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
public class PairEndpointData {
    private String name;
    private String url;
    private Date lastDate;

    public PairEndpointData(String name, String url, Date date) {
        this.name = name;
        this.url = url;
        this.lastDate = date;
    }

    public Date receiveNotFormattedLastDate() { // not add get, class automatically converted to json
        return lastDate;
    }

    public String getLastDate() {
        if (lastDate.equals(new Date(0)))
            return "EMPTY";
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssXX");
        return dateFormat.format(lastDate);
    }
}
