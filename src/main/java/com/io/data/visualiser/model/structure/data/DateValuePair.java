package com.io.data.visualiser.model.structure.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DateValuePair {
    private Date date;
    private String value;


    public String getDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssXX");
        return dateFormat.format(date);
    }

    public Date receiveNotFormattedDate(){ //do not add get, class automatically converted to json
        return date;
    }

}
