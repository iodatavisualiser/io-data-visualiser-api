package com.io.data.visualiser.model.structure.data;

import lombok.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NameDataValue {
    private String name;
    private String value;
    private Date date;

    public String getDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssXX");
        return dateFormat.format(date);
    }
    public Date receiveNotFormattedDate(){ // not add get, class automatically converted to json
        return date;
    }

}
