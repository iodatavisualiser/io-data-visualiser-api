package com.io.data.visualiser.model.cache;

import com.io.data.visualiser.model.structure.data.DateInterval;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class IntervalList {
    final private List<DateInterval> intervals;

    public IntervalList() {
        this.intervals = new LinkedList<>();
    }

    public boolean isCovert(DateInterval inter){
        for (DateInterval current : intervals) {
            if (current.to.compareTo(inter.to)>=0 && current.from.compareTo(inter.from)<=0)
                return true;
        }
        return false;
    }

    public void addInterval(DateInterval inter){
        ListIterator<DateInterval> iterator = intervals.listIterator();
        while(iterator.hasNext()){
            DateInterval current = iterator.next();
            if(overlap(inter,current)){
                inter = merge(inter,current);
                iterator.remove();
            }
        }
        intervals.add(inter);
    }

    public Date getEnd(DateInterval inter){
        for (DateInterval current : intervals){
            if (current.to.compareTo(inter.to)>=0 && current.to.compareTo(inter.from)<=0){
                return current.from;
            }
        }
        return inter.to;
    }

    private DateInterval merge(DateInterval inter1, DateInterval inter2){
        Date minD = inter1.from;
        if(inter2.from.before(minD))
            minD = inter2.from;
        Date maxD = inter1.to;
        if(inter2.to.after(minD))
            maxD = inter2.to;
        return new DateInterval(minD,maxD);
    }

    private boolean overlap(DateInterval inter1, DateInterval inter2){
        return ((inter1.from.compareTo(inter2.from)>=0 && inter1.from.compareTo(inter2.to)<=0) ||
                (inter2.from.compareTo(inter1.from)>=0 && inter2.from.compareTo(inter1.to)<=0));
    }

}
