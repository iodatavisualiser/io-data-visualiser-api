package com.io.data.visualiser.model.configuration;

import com.io.data.visualiser.model.structure.data.Device;
import com.io.data.visualiser.model.structure.data.Station;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;


@Data
@NoArgsConstructor
public class Configuration {
    private List<Station> stations;
    private String[] sensorNameArray;
    private List<Device> sensors;
    private int maxMonthRange;

    public List<String> getParameters(){
        List<String> param = new ArrayList<>(sensors.size());
        for(Device dev: sensors)
            param.add(dev.getName());
        return param;
    }

    public void setSensors(List<Device> sensors) {
        this.sensors = sensors;
        int i=0;
        sensorNameArray = new String[sensors.size()];
        for(Device d:sensors){
            sensorNameArray[i] = d.getName();
            i+=1;
        }
    }

}

