package com.io.data.visualiser.model.structure.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EndpointData {
    private String name;
    private List<NameDataValue> data;

}
