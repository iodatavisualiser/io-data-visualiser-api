package com.io.data.visualiser.model.structure.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.io.data.visualiser.model.structure.data.EndpointData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CacheRecentDataResponse implements ErrorsHandlingResponseI {
    @JsonUnwrapped
    List<EndpointData> data;
    String[] errorsMessages;
    @JsonIgnore
    List<Throwable> errors;

}
