package com.io.data.visualiser.model.structure.response;

import java.util.List;

public interface ErrorsHandlingResponseI {
    List<Throwable> getErrors();
    String[] getErrorsMessages();
    List<?> getData();
}
