package com.io.data.visualiser.model.cache;

import com.io.data.visualiser.model.configuration.ConfigurationProvider;
import com.io.data.visualiser.model.structure.data.DateInterval;
import com.io.data.visualiser.model.structure.data.DateValuePair;
import com.io.data.visualiser.model.structure.data.NameDataValue;
import com.io.data.visualiser.model.structure.response.CacheDataResponse;
import com.io.data.visualiser.service.DatahubRequest;
import com.io.data.visualiser.service.DatahubRequester;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class Cache {
    static final Logger LOGGER = LoggerFactory.getLogger(Cache.class);

    private final Map<String, ConcurrentHashMap<Date, String>> params; //TODO concurrent hashmap
    @Setter @Getter
    private String endpoint;
    @Setter @Getter
    private String id = "";
    private Date minD;
    private Date maxD;
    final private IntervalList intervals;

    public Cache(List<String> par, String endpoint) {
        this.endpoint = endpoint;
        params = new HashMap<>();
        for (String s : par) {
            params.put(s, new ConcurrentHashMap<>());
        }
        intervals = new IntervalList();
        minD = new Date(0);
        maxD = new Date(0);
    }

    public void add(String param, Date date, String value) {
        params.get(param).put(date, value);
        if (minD.after(date))
            minD = date;
        if (maxD.before(date))
            maxD = date;
    }

    public CacheDataResponse getData(String param, Date from, Date to) {
        Map<Date, String> analyzed = params.get(param);
        if (analyzed == null ) {
            throw new IllegalArgumentException("param is not in conf:: " + param);
        }

        boolean covert = true;
        List<Throwable> errors = new LinkedList<>();
        if (!intervals.isCovert(new DateInterval(from, to))) {
            covert = false;
            minD = from;
            maxD = new Date(0);
            Date laterDate = intervals.getEnd(new DateInterval(from, to));
            Future<DatahubRequest.DhReqResult> futureID = DatahubRequester.req(
                    ConfigurationProvider.getConfiguration().getSensorNameArray(),
                    endpoint,
                    from,
                    laterDate);
            try {
                String tmpId = futureID.get().getId();
                id = tmpId != null ? tmpId : id;
                errors = futureID.get().getErrors();
            } catch (InterruptedException | ExecutionException e) {
                LOGGER.error("Datahub timeout::" + e.getMessage());
                errors.add(e);
            }
        }
        List<DateValuePair> result = new LinkedList<>();

        for (Date date : analyzed.keySet()) {
            if (date.compareTo(from) >= 0 && date.compareTo(to) <= 0) {
                result.add(new DateValuePair(date, analyzed.get(date)));
            }
        }
        if (!covert) {
            intervals.addInterval(new DateInterval(minD, maxD));
        }

        return new CacheDataResponse(id,
                from,
                to,
                param,
                result,
                errors
                    .stream()
                    .map(Throwable::getMessage)
                    .toArray(String[]::new),
                errors
        );
    }

    public List<NameDataValue> getRecent() {
        List<NameDataValue> result = new ArrayList<>(params.size());
        for (String name : params.keySet()) {
            Map<Date, String> map = params.get(name);
            String val = "";
            Date date = new Date(0);
            for (Date d : map.keySet()) {
                if (d.after(date)) {
                    val = map.get(d);
                    date = d;
                }
            }
            result.add(new NameDataValue(name, val, date));
        }
        return result;
    }

    public Date getLastDate() {
        Date oldest = new Date(0);
        for (String s : params.keySet()) {
            for (Date d : params.get(s).keySet()) {
                if (oldest.before(d))
                    oldest = d;
            }
        }
        return oldest;
    }

}
