package com.io.data.visualiser.model.structure.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Device {
    private String name;
    private String defaultUnit;
    private ArrayList<String> possibleUnits;

}
