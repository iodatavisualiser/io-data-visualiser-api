package com.io.data.visualiser.model.structure.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.io.data.visualiser.model.structure.data.DateValuePair;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@lombok.Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CacheDataResponse implements ErrorsHandlingResponseI{
    String id;
    Date from;
    Date to;
    String type;
    @JsonUnwrapped
    List<DateValuePair> data;
    String[] errorsMessages;
    @JsonIgnore
    List<Throwable> errors;

    public String getFrom() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssXX");
        return dateFormat.format(from);
    }

    public String getTo() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssXX");
        return dateFormat.format(to);
    }
}
