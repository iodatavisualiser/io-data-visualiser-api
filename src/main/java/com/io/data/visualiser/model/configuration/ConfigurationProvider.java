package com.io.data.visualiser.model.configuration;

import com.io.data.visualiser.model.cache.EndpointCaches;
import com.io.data.visualiser.model.structure.data.Device;
import com.io.data.visualiser.model.structure.data.Station;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class ConfigurationProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationProvider.class);
    private static Configuration configuration;

    @Getter @Setter
    private static EndpointCaches endpointCaches;

    public static Configuration getConfiguration() {
        return configuration;
    }

    public static void setConfiguration(Configuration configuration) {
        ConfigurationProvider.configuration = configuration;
        List<String> urls = new ArrayList<>(configuration.getStations().size());
        for (Station s : configuration.getStations())
            urls.add(s.getEndpoint());
        List<String> params = new ArrayList<>(configuration.getSensors().size());
        for (Device d : configuration.getSensors())
            params.add(d.getName());
        endpointCaches = new EndpointCaches(urls, params);
        LOGGER.info("configuration set");
    }

}
