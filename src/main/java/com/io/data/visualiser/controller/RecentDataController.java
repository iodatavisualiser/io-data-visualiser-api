package com.io.data.visualiser.controller;


import com.io.data.visualiser.model.configuration.ConfigurationProvider;
import com.io.data.visualiser.model.structure.response.CacheRecentDataResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class RecentDataController {
    @RequestMapping(method = RequestMethod.GET, path = "/recent")
    static public ResponseEntity<Object> getResponse() {
        try {
            CacheRecentDataResponse cacheRecentDataResponse = ConfigurationProvider.getEndpointCaches().getRecent();
            return ResponseBuilder.buildResponse(cacheRecentDataResponse);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
