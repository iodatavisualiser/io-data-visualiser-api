package com.io.data.visualiser.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.io.data.visualiser.model.configuration.Configuration;
import com.io.data.visualiser.model.configuration.ConfigurationProvider;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class ConfigurationController {

    @RequestMapping(value = "/config", method = RequestMethod.POST)
    public ResponseEntity<String> getConfig(@RequestBody String payload) throws JsonProcessingException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            Configuration configuration = mapper.readValue(payload, Configuration.class);
            ConfigurationProvider.setConfiguration(configuration);
            return new ResponseEntity<>("{}", HttpStatus.OK);

        } catch (JsonProcessingException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
