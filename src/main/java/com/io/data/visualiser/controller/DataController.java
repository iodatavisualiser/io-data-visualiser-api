package com.io.data.visualiser.controller;

import com.io.data.visualiser.model.configuration.ConfigurationProvider;
import com.io.data.visualiser.model.structure.response.CacheDataResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.DateTimeException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class DataController {

    @RequestMapping(method = RequestMethod.GET, path = "/response")
    public static ResponseEntity<Object> getResponse(@RequestParam String url,
                                                     @RequestParam String from,
                                                     @RequestParam String to,
                                                     @RequestParam String param) {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssz");
            ZonedDateTime fromT = ZonedDateTime.parse(from, formatter);
            ZonedDateTime toT = ZonedDateTime.parse(to, formatter);
            Date fromD = Date.from(fromT.toInstant());
            Date toD = Date.from(toT.toInstant());
            CacheDataResponse cacheDataResponse = ConfigurationProvider.getEndpointCaches().getData(url, fromD, toD, param);
            return ResponseBuilder.buildResponse(cacheDataResponse);

        } catch (DateTimeException | IllegalArgumentException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }




}
