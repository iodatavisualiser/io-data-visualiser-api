package com.io.data.visualiser.controller;

import com.io.data.visualiser.model.structure.response.ErrorsHandlingResponseI;
import com.io.data.visualiser.service.DatahubReqException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseBuilder {
    public static ResponseEntity<Object> buildResponse(ErrorsHandlingResponseI cacheResponse) {

        if (cacheResponse.getData().size() == 0) {
            if (cacheResponse.getErrors().stream().anyMatch(e -> e instanceof DatahubReqException)) {
                return new ResponseEntity<>(cacheResponse, HttpStatus.SERVICE_UNAVAILABLE);
            } else if (cacheResponse.getErrors().stream().anyMatch(e -> e instanceof Exception)) {
                return new ResponseEntity<>(cacheResponse, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<>(cacheResponse, HttpStatus.OK);
    }
}
