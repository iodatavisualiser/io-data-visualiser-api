package com.io.data.visualiser.controller;

import com.io.data.visualiser.model.structure.response.CacheInactiveListResponse;
import com.io.data.visualiser.model.configuration.ConfigurationProvider;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class InactiveEndpointsController {
    @RequestMapping(method = RequestMethod.GET, path = "/inactive")
    static public ResponseEntity<Object> getInactive() {
        try {
            CacheInactiveListResponse cacheInactiveListResponse = ConfigurationProvider.getEndpointCaches().getInactive();
            return ResponseBuilder.buildResponse(cacheInactiveListResponse);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}

