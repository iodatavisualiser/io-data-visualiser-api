package com.io.data.visualiser;

import java.io.IOException;
import java.io.InputStream;

public class TestHelper {
    public static String readExampleConfig(String path) throws IOException {
        try (InputStream in = TestHelper.class.getClassLoader().getResourceAsStream(path)) {
            assert in != null;
            return new String(in.readAllBytes());
        }
    }

    public static String readExampleConfig() throws IOException {
        return readExampleConfig("testConfig.json");
    }

    public static void printFooter() {
        String f = ">".repeat(100);
        int n = 3;
        for (int i = 0; i < n; i++) {
            System.out.println(f);
        }
    }
}
