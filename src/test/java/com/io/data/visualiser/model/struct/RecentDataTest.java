package com.io.data.visualiser.model.struct;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.io.data.visualiser.TestHelper;
import com.io.data.visualiser.controller.RecentDataController;
import com.io.data.visualiser.model.configuration.ConfigFileReader;
import com.io.data.visualiser.model.configuration.Configuration;
import com.io.data.visualiser.model.configuration.ConfigurationProvider;
import com.io.data.visualiser.service.DatahubRequester;
import com.io.data.visualiser.model.structure.response.CacheRecentDataResponse;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.*;

public class RecentDataTest {
    @BeforeAll
    static void setup() {
        DatahubRequester.restart();
    }
    @AfterAll
    public static void tearDown() {
        DatahubRequester.shutdown();
    }
    @Test
    public void t() throws IOException {
            String result = TestHelper.readExampleConfig();
            Configuration conf = ConfigFileReader.readConfiguration(result);
            ConfigurationProvider.setConfiguration(conf);

            TestHelper.printFooter();

            CacheRecentDataResponse s = (CacheRecentDataResponse) RecentDataController.getResponse().getBody();
            ObjectMapper mapper = new ObjectMapper();
            String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(s);
            System.out.println(jsonString);

            TestHelper.printFooter();
    }
}
