package com.io.data.visualiser.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.io.data.visualiser.TestHelper;
import com.io.data.visualiser.model.cache.Cache;
import com.io.data.visualiser.model.configuration.ConfigFileReader;
import com.io.data.visualiser.model.configuration.ConfigurationProvider;
import com.io.data.visualiser.service.DatahubRequester;
import com.io.data.visualiser.model.configuration.Configuration;
import com.io.data.visualiser.model.structure.data.DateValuePair;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CachesTest {
    @BeforeAll
    static void setup() {
        DatahubRequester.restart();
    }
    @AfterAll
    static void tearDown() {
        DatahubRequester.shutdown();
    }

    @Test
    public void t(){
        Configuration c = new Configuration();
        c.setStations(new ArrayList<>());
        c.setSensors(new ArrayList<>());
        String s = "sensors/mainSensorModule/sensorData/gps/alt";
        String s2 = "sensors/mainSensorModule/sensorData/gps/lat";
        c.setSensorNameArray(new String[]{s,s2});
        ConfigurationProvider.setConfiguration(c);
        ArrayList<String> l = new ArrayList<>(1);
        l.add(s);
        l.add(s2);
        Cache cache = new Cache(l,"https://datahub.ki.agh.edu.pl/api/endpoints/78/data");
        ZonedDateTime zonedDateTimeOf2 = ZonedDateTime.of(2022, 5, 7, 18, 0, 0, 0, ZoneId.of("UTC"));
        ZonedDateTime zonedDateTimeOf = ZonedDateTime.of(2022, 5, 8, 0, 0, 0, 0, ZoneId.of("UTC"));
        List<DateValuePair> dd = cache.getData(s,Date.from(zonedDateTimeOf2.toInstant()),Date.from(zonedDateTimeOf.toInstant())).getData();
        TestHelper.printFooter();
        for(DateValuePair dp: dd){
            System.out.println(dp.getDate());
            System.out.println(dp.getValue());
        }
        TestHelper.printFooter();

        dd = cache
                .getData(
                        s2,
                        Date.from(zonedDateTimeOf2.toInstant()),
                        Date.from(zonedDateTimeOf.toInstant()))
                .getData();
        TestHelper.printFooter();

        for(DateValuePair dp: dd){
            System.out.println(dp.getDate());
            System.out.println(dp.getValue());
        }
        TestHelper.printFooter();

        zonedDateTimeOf2 = ZonedDateTime.of(2022, 5, 1, 10, 0, 0, 0, ZoneId.of("UTC"));
        zonedDateTimeOf = ZonedDateTime.of(2022, 5, 9, 0, 0, 0, 0, ZoneId.of("UTC"));
        dd = cache
                .getData(
                        s2,
                        Date.from(zonedDateTimeOf2.toInstant()),
                        Date.from(zonedDateTimeOf.toInstant()))
                .getData();
        TestHelper.printFooter();
        for(DateValuePair dp: dd){
            System.out.println(dp.getDate());
            System.out.println(dp.getValue());
        }
        TestHelper.printFooter();
    }

    static class ConfigFileReaderTest {
        @Test
        public void t() throws IOException {
                String result = TestHelper.readExampleConfig();
                Configuration conf = ConfigFileReader.readConfiguration(result);
                ObjectMapper mapper = new ObjectMapper();
                String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(conf);
                System.out.println(jsonString);
        }

    }
}
