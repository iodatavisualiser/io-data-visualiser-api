package com.io.data.visualiser.model;

import com.io.data.visualiser.TestHelper;
import com.io.data.visualiser.model.configuration.ConfigFileReader;
import com.io.data.visualiser.model.configuration.ConfigurationProvider;
import com.io.data.visualiser.model.configuration.Configuration;
import com.io.data.visualiser.model.structure.data.PairEndpointData;
import com.io.data.visualiser.model.structure.response.CacheInactiveListResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.*;

public class InactiveEndpointsTest {
    @Test
    public void t() throws IOException {
            String result = TestHelper.readExampleConfig();
            Configuration conf = ConfigFileReader.readConfiguration(result);
            ConfigurationProvider.setConfiguration(conf);
            CacheInactiveListResponse inl = ConfigurationProvider.getEndpointCaches().getInactive();
            for (PairEndpointData epd : inl.getData()) {
                System.out.println(epd.getName() + " " + epd.getUrl() + " " + epd.getLastDate());
            }
            Assertions.assertEquals(2, inl.getData().size());

    }
}
