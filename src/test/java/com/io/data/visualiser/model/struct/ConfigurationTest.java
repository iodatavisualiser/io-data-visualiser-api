package com.io.data.visualiser.model.struct;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.io.data.visualiser.model.configuration.Configuration;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;

class ConfigurationTest {
    @Test
    public void t1() throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        try (InputStream in = getClass().getClassLoader().getResourceAsStream("example-config.json")) {
            assert in != null;
            String jsonString = new String(in.readAllBytes());
            Configuration student = mapper.readValue(jsonString, Configuration.class);

            String nJsonString = mapper
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(student);

            System.out.println(student);
            System.out.println(jsonString);
            System.out.println(nJsonString);
        }
    }
}