package com.io.data.visualiser.model.struct;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.io.data.visualiser.TestHelper;
import com.io.data.visualiser.controller.DataController;
import com.io.data.visualiser.model.configuration.ConfigFileReader;
import com.io.data.visualiser.model.configuration.Configuration;
import com.io.data.visualiser.model.configuration.ConfigurationProvider;
import com.io.data.visualiser.model.structure.response.CacheDataResponse;
import com.io.data.visualiser.service.DatahubRequester;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.*;

public class ResponseTest {
    @BeforeAll
    static void setup() {
        DatahubRequester.restart();
    }
    @AfterAll
    static void tearDown() {
        DatahubRequester.shutdown();
    }

    @Test
    public void t() throws IOException {
            String result = TestHelper.readExampleConfig();
            Configuration conf = ConfigFileReader.readConfiguration(result);
            ConfigurationProvider.setConfiguration(conf);

            TestHelper.printFooter();
            CacheDataResponse s = (CacheDataResponse) DataController.getResponse(
                    "https://datahub.ki.agh.edu.pl/pl/datasets/env-mon-outskirts-of-krakow/endpoints/87/data/",
                    "2022-04-11T16:22:57+02:00",
                    "2022-04-10T15:22:57+02:00",
                    "particleConcentrationSensor/concentration/pm1"
            ).getBody();
            ObjectMapper mapper = new ObjectMapper();
            String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(s);
            System.out.println(jsonString);
            TestHelper.printFooter();
    }

}
