package com.io.data.visualiser.service;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import java.io.IOException;
import java.net.InetAddress;
import java.time.ZonedDateTime;
import java.util.concurrent.Future;

public class DatahubRequsetTest {
    @BeforeAll
    public static void checkDatahubNetworkReachability() throws IOException {
        DatahubRequester.restart();
        String[] ipAddresses = new String[] {"10.130.31.206", "10.129.11.212"};
        boolean reachable = false;
        int timeout = 5000;
        for (String s : ipAddresses) {
            InetAddress inet = InetAddress.getByName(s);
            if (inet.isReachable(timeout)) {
                reachable = true;
                break;
            }
        }
        Assertions.assertTrue(reachable,
                "could not reach datahub network");
    }

    @AfterAll
    static void tearDown() {
        DatahubRequester.shutdown();
    }

    @Test
    public void correctReqTest_1() throws Exception {
        String[] params = new String[]{"heater/tempSet"};
        Future<DatahubRequest.DhReqResult> res = DatahubRequester.req(
                params,
                "https://datahub.ki.agh.edu.pl/api/endpoints/73/data/",
                25,
                25);
        System.out.println(res.get());
    }

    @Test
    public void correctReqTest_2() throws Exception {
        // TODO make date range dependent on current date;
        String[] params = new String[]{"heater/tempSet"};
        ZonedDateTime dateTo = ZonedDateTime.parse("2022-05-09T18:13:21.092012+02:00");
        ZonedDateTime dateFrom = ZonedDateTime.parse("2022-05-07T18:13:21.092012+02:00");

        Future<DatahubRequest.DhReqResult> res = DatahubRequester.req(params,
                "https://datahub.ki.agh.edu.pl/api/endpoints/73/data/", dateFrom, dateTo);
        System.out.println(res.get());
    }

}
