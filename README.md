Java version - 11 Oracle Open JDK

### Tests
preconditions: 
1. application running within port 8080 <br>

###### Manual test 1
simple test using bash script (test.sh) in resources directory<br>
output in file 'tmp.test.res' or in one specified via flag [-o|--output-file]
1. post configuration and ask for cacheDataResponse. 
It is required to specify a type of request: 'response', 'recent', 'inactive' or 'conf'.<br>
If conf is specified only configuration is posted otherwise posting conf is followed by request for data (unless [-n|--no-conf] is specified)
```bash
./test.sh [response|recent|inactive|conf]
```
2. only ask for data
```bash
./test.sh [response|recent|inactive|conf] [--no-conf|-n]
```
3. if using 'response' type you can add some pollution to request part in order to test exception handling
```bash
./test.sh response [[-c|--conf-pollution]|[-p|--url-pollution]|[-P|--param-pollution]|[-D|--data-pollution]] {someString}
```
